package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
)

type Edge struct {
	source, target byte
}

func main() {
	lines := make([]string, 0)
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	unsorted_steps := make(map[byte]bool)
	var es []Edge
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		lines = append(lines, line)
		words := strings.Split(line, " ")
		e := Edge{words[1][0], words[7][0]}
		es = append(es, e)
		unsorted_steps[e.source] = true
		unsorted_steps[e.target] = true
	}

	var steps []byte
	for s, _ := range unsorted_steps {
		steps = append(steps, s)
	}
	sort.Slice(steps, func(i, j int) bool {
		return steps[i] < steps[j]
	})
	fmt.Println(steps)

	// The requires map maps from each step to
	// (a slice of) the steps that must be completed before
	// this one.
	requires := make(map[byte][]byte)
	for _, e := range es {
		requires[e.target] = append(requires[e.target], e.source)
	}
	fmt.Println(requires)

	// The completed steps
	completed := make(map[byte]bool)
	// Scan all incomplete steps in alphabetical order,
	// the first one whose required steps are all completed
	// is the next step to complete.
	for {
		for _, step := range steps {
			if completed[step] {
				continue
			}
			// all required steps completed?
			good := true
			for _, need := range requires[step] {
				if !completed[need] {
					good = false
					break
				}
			}
			if good {
				fmt.Print(string(step))
				completed[step] = true
				break
			}
		}
		if len(completed) == len(steps) {
			break
		}
	}
	fmt.Print("\n")
}
