package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)
	input, err := inp.ReadBytes('\n')
	if err != nil {
		log.Fatal(err)
	}
	if input[len(input)-1] == '\n' {
		input = input[:len(input)-1]
	}
	fmt.Println(input)

	for {
		reduced := false
		input, reduced = reduce_pass(input)
		if !reduced {
			break
		}
	}
	fmt.Println(len(input))
}

func reduce_pass(bs []byte) ([]byte, bool) {
	var res []byte
	reduced := false
	// note: when pairs react i advances twice
	for i := 0; i < len(bs); i += 1 {
		if i < len(bs)-1 &&
			bs[i]^bs[i+1] == 32 {
			reduced = true
			i += 1
			continue
		}
		res = append(res, bs[i])
	}
	return res, reduced
}
