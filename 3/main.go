package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

var Fabric [1000][1000]int

type Rect struct {
	id            int
	left, top     int
	width, height int
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	var rs []Rect
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		var r Rect
		fmt.Sscanf(line, "#%d @ %d,%d: %dx%d", &r.id, &r.left, &r.top, &r.width, &r.height)
		rs = append(rs, r)
	}

	for i := 0; i < len(rs); i += 1 {
		claim(rs[i])
	}

	total := 0
	for y := 0; y < len(Fabric); y += 1 {
		for x := 0; x < len(Fabric[0]); x += 1 {
			if Fabric[y][x] > 1 {
				total += 1
			}
		}
	}
	fmt.Println(total)
}

func claim(r Rect) {
	for y := r.top; y < r.top+r.height; y += 1 {
		for x := r.left; x < r.left+r.width; x += 1 {
			Fabric[y][x] += 1
		}
	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// return the number of square inches in the intersection of a and b
func intersect(a, b Rect) int {
	// Find the rightmost left-hand edge,
	// and the leftmost right-hand edge.
	l := max(a.left, b.left)
	r := min(a.left+a.width, b.left+b.width)
	if r <= l {
		return 0
	}
	// Find the bottommost top edge,
	// and the topmost bottom edge.
	top := max(a.top, b.top)
	bot := min(a.top+a.height, b.top+b.height)
	if bot <= top {
		return 0
	}
	return (r - l) * (bot - top)
}
