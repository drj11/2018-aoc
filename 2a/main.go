package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	var ids []string
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		ids = append(ids, line)
	}

	for i := 0; i < len(ids); i += 1 {
		for j := i + 1; j < len(ids); j += 1 {
			c := count_diffs(ids[i], ids[j])
			if c == 1 {
				fmt.Println(common(ids[i], ids[j]))
			}
		}
	}
}

// string of letters common to a and b
func common(a, b string) string {

	r := ""
	for i := 0; i < len(a); i += 1 {
		if a[i] == b[i] {
			r += a[i : i+1]
		}
	}
	return r
}
func count_diffs(a, b string) int {
	d := 0
	for i := 0; i < len(a); i += 1 {
		if a[i] != b[i] {
			d += 1
		}
	}
	return d
}

// Return a 0 or 1 according to whether
// string s has a letter that occurs exactly 2 or 3 times.
func twos_threes(s string) (twos int, threes int) {
	count := make(map[rune]int)
	for _, c := range s {
		count[c] += 1
	}
	for _, v := range count {
		if v == 2 {
			twos = 1
		}
		if v == 3 {
			threes = 1
		}
	}
	return
}
