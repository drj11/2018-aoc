package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {

	c := make(chan int)
	go func(put chan<- int) {
		for {
			file, err := os.Open("input")
			if err != nil {
				log.Fatal(err)
			}
			inp := bufio.NewReader(file)
			for {
				line, err := inp.ReadString('\n')
				if err == io.EOF {
					break
				}
				if err != nil {
					log.Fatal(err)
				}
				var i int
				fmt.Sscanf(line, "%d", &i)
				put <- i
			}
		}
	}(c)

	seen := make(map[int]bool)

	adjustments := (<-chan int)(c)
	freq := 0
	for adj := range adjustments {
		freq += adj
		if seen[freq] {
			break
		}
		seen[freq] = true
	}
	fmt.Println(freq)
}
