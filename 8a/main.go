package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

type Node struct {
	children []*Node
	meta     []int
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	ch := make(chan int)

	go func(c chan<- int) {
		for {
			var number int
			_, err := fmt.Fscan(inp, &number)
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatal(err)
			}
			c <- number
		}
		close(c)
	}(ch)

	node := read_node(ch)
	fmt.Println(value(node))
}

func read_node(c <-chan int) *Node {
	node := Node{}
	n_children := <-c
	n_meta := <-c
	for i := 0; i < n_children; i += 1 {
		node.children = append(node.children, read_node(c))
	}
	for i := 0; i < n_meta; i += 1 {
		node.meta = append(node.meta, <-c)
	}
	return &node
}

func value(node *Node) int {
	sum := 0
	if len(node.children) == 0 {
		for _, m := range node.meta {
			sum += m
		}
		return sum
	}
	for _, m := range node.meta {
		m -= 1
		if 0 <= m && m < len(node.children) {
			sum += value(node.children[m])
		}
	}
	return sum
}
