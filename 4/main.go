package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
)

// Records one hour of sleep/wake. false = wake, true = sleep
type Hour [60]bool

type Guard struct {
	id     int
	asleep []Hour
}

func (g *Guard) totalSleep() int {
	total := 0
	for _, hour := range g.asleep {
		for _, asleep := range hour {
			if asleep {
				total += 1
			}
		}
	}
	return total
}

func main() {
	lines := make([]string, 0)
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		lines = append(lines, line)
	}

	sort.Strings(lines)

	blocks := make(chan []string)
	go eachGuard(lines, blocks)

	guards := make(map[int]*Guard)

	for block := range blocks {
		idline := block[0]
		var id int
		fmt.Sscanf(idline[26:], "%d", &id)
		_, ok := guards[id]
		if !ok {
			guards[id] = &Guard{id: id}
		}
		guard := guards[id]
		sleep := Sleepy(block[1:])
		guard.asleep = append(guard.asleep, sleep)
	}

	max_sleep := -1
	var sleepy_guard *Guard
	for _, guard := range guards {
		sleep := guard.totalSleep()
		if sleep > max_sleep {
			max_sleep = sleep
			sleepy_guard = guard
			fmt.Println(max_sleep, sleepy_guard.id)
		}
	}

	// Count most sleepy minute
	var count [60]int
	max_count := -1
	max_minute := -1
	for _, hour := range sleepy_guard.asleep {
		for i, asleep := range hour {
			if asleep {
				count[i] += 1
				if count[i] > max_count {
					max_count = count[i]
					max_minute = i
				}
			}
		}
	}
	fmt.Println(sleepy_guard.id, max_minute, sleepy_guard.id*max_minute)
}

// Process the lines and return an Hour representing which
// minutes are noted as asleep.
func Sleepy(lines []string) Hour {
	var hour Hour
	// loop two at a time
	for i := 0; i < len(lines); i += 2 {
		sleep := lines[i]
		wake := lines[i+1]
		var i, j int
		fmt.Sscanf(sleep[15:], "%d", &i)
		fmt.Sscanf(wake[15:], "%d", &j)
		for ; i < j; i += 1 {
			hour[i] = true
		}
	}
	return hour
}

// Split the slice into a series of slices,
// one slice per guard. Each output slice will begin with
// "Guard XXX begins shift".
func eachGuard(lines []string, c chan<- []string) {
	var block []string
	for _, line := range lines {
		if strings.Contains(line, "Guard") {
			if len(block) > 0 {
				c <- block
			}
			block = nil
		}
		block = append(block, line)
	}
	if len(block) > 0 {
		c <- block
	}
	close(c)
}
