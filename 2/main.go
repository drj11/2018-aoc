package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	twos := 0
	threes := 0

	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		two, three := twos_threes(line)
		twos += two
		threes += three
	}

	fmt.Println(twos * threes)
}

// Return a 0 or 1 according to whether
// string s has a letter that occurs exactly 2 or 3 times.
func twos_threes(s string) (twos int, threes int) {
	count := make(map[rune]int)
	for _, c := range s {
		count[c] += 1
	}
	for _, v := range count {
		if v == 2 {
			twos = 1
		}
		if v == 3 {
			threes = 1
		}
	}
	return
}
