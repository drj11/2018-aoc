package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)
	input, err := inp.ReadBytes('\n')
	if err != nil {
		log.Fatal(err)
	}
	if input[len(input)-1] == '\n' {
		input = input[:len(input)-1]
	}
	fmt.Println(input)

	min_polymer := 999999999
	letter_removed := byte('$')
	for _, b := range []byte("abcdefghijklmnopqrstuvwxyz") {
		trial_polymer := remove(input, b)
		reduced_polymer := reduce_fully(trial_polymer)
		if len(reduced_polymer) < min_polymer {
			min_polymer = len(reduced_polymer)
			letter_removed = b
			fmt.Println(b, min_polymer)
		}
	}
	fmt.Println(min_polymer, rune(letter_removed))
}

// delete d from bs, returning a fresh slice
// d is treated as a case-insensitive ASCII byte,
// so when d == 't', both 't' and 'T' will be removed.
func remove(bs []byte, d byte) []byte {
	var res []byte
	for _, b := range bs {
		if b|32 == d|32 {
			continue
		}
		res = append(res, b)
	}
	return res
}

func reduce_fully(bs []byte) []byte {
	for {
		reduced := false
		bs, reduced = reduce_pass(bs)
		if !reduced {
			return bs
		}
	}
}

func reduce_pass(bs []byte) ([]byte, bool) {
	var res []byte
	reduced := false
	// note: when pairs react i advances twice
	for i := 0; i < len(bs); i += 1 {
		if i < len(bs)-1 &&
			// ASCII, yeah!
			bs[i]^bs[i+1] == 32 {
			reduced = true
			i += 1
			continue
		}
		res = append(res, bs[i])
	}
	return res, reduced
}
