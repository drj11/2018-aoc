package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
)

type Edge struct {
	source, target byte
}

func main() {
	lines := make([]string, 0)
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	unsorted_steps := make(map[byte]bool)
	var es []Edge
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		lines = append(lines, line)
		words := strings.Split(line, " ")
		e := Edge{words[1][0], words[7][0]}
		es = append(es, e)
		unsorted_steps[e.source] = true
		unsorted_steps[e.target] = true
	}

	var steps []byte
	for s, _ := range unsorted_steps {
		steps = append(steps, s)
	}
	sort.Slice(steps, func(i, j int) bool {
		return steps[i] < steps[j]
	})

	// The requires map maps from each step to
	// (a slice of) the steps that must be completed before
	// this one.
	requires := make(map[byte][]byte)
	for _, e := range es {
		requires[e.target] = append(requires[e.target], e.source)
	}
	for _, target := range steps {
		s := ""
		for _, r := range requires[target] {
			s = s + string(r)
		}
		fmt.Println(string(target), "after", s)
	}

	// Steps that have been started and finished respectively.
	// Initially, none.
	started := make(map[byte]bool)
	finished := make(map[byte]bool)
	starts := "" // same as started in string form
	// Number of ticks passed
	tick := 0

	type Worker struct {
		id         int
		step       byte
		completing int // time when current step is complete
	}

	workers := make([]Worker, 5)
	for i := range workers {
		workers[i].id = i
	}
	_ = tick

	// Scan all steps that have not been worked on in
	// alphabetical order,
	// the first one whose required steps are all completed
	// is the next step to start.
	next_step := func() byte {
		for _, step := range steps {
			if started[step] {
				continue
			}
			// all required steps finished?
			good := true
			for _, need := range requires[step] {
				if !finished[need] {
					good = false
					break
				}
			}
			if good {
				return step
			}
		}
		return 0
	}

	// a) If no available worker, wait (advance) until
	// next worker available.
	// b) When worker available, if no available steps,
	// wait (advance) until next worker available.
	// c) If available worker and available steps,
	// task worked with alphabetically least step.
	// Note: a) and b) above both result in same thing:
	// advance until a new worker becomes avilable.

	available_worker := func() *Worker {
		for i := range workers {
			if workers[i].completing <= tick {
				return &workers[i]
			}
		}
		return nil
	}

	fmt_workers := func() {
		fmt.Printf("%04d ", tick)
		for _, w := range workers {
			if w.completing > tick {
				fmt.Printf("%s ", string(w.step))
			} else {
				fmt.Print(". ")
			}
		}
		fmt.Print("\n")
	}

	assign_worker := func(w *Worker, step byte) {
		if w.completing > tick {
			log.Fatal("assigning unavailable worker")
		}
		w.step = step
		// ASCII hack. 'A' = 65
		w.completing = tick + int(step) - 4
		started[step] = true
		starts = starts + string(w.step)
	}

	complete_earliest_step := func() {
		min_completing := 999999999
		for _, w := range workers {
			if w.completing > tick && w.completing < min_completing {
				min_completing = w.completing
			}
		}
		if min_completing == 999999999 {
			log.Fatal("No completable steps found")
		}
		tick = min_completing
		for _, w := range workers {
			if w.completing == tick {
				finished[w.step] = true
				// fmt.Println("worker", i, "finishing", string(w.step), "at", tick)
			}
		}
	}

	for {
		if len(finished) == len(steps) {
			break
		}
		for {
			step := next_step()
			worker := available_worker()
			if worker == nil || step == 0 {
				break
			}
			assign_worker(worker, step)
		}
		fmt_workers()
		complete_earliest_step()
	}
	fmt_workers()
	fmt.Println(starts)
	fmt.Println(tick)
}
