package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

const N = 360

var Fabric [N][N]int

type Point struct {
	id   int
	x, y int
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	var ps []Point
	id := 1
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		p := Point{id: id}
		fmt.Sscanf(line, "%d,%d", &p.x, &p.y)
		ps = append(ps, p)
		id += 1
	}

	for y := 0; y < len(Fabric); y += 1 {
		for x := 0; x < len(Fabric[0]); x += 1 {
			dangers := nearest(x, y, ps)
			if len(dangers) == 1 {
				Fabric[y][x] = dangers[0].id
			}
		}
	}

	// Scan the edges to find the infinite regions
	baddies := make(map[int]bool)
	for x := 0; x < len(Fabric[0]); x += 1 {
		baddies[Fabric[0][x]] = true
		baddies[Fabric[len(Fabric)-1][x]] = true
	}
	for y := 0; y < len(Fabric[0]); y += 1 {
		baddies[Fabric[y][0]] = true
		baddies[Fabric[y][len(Fabric[0])-1]] = true
	}

	// Count region sizes
	count := make(map[int]int)
	for y := 0; y < len(Fabric); y += 1 {
		for x := 0; x < len(Fabric[0]); x += 1 {
			c := Fabric[y][x]
			if baddies[c] {
				continue
			}
			count[c] += 1
		}
	}

	fmt.Println(count)
	var max_count = -1
	for _, size := range count {
		if size > max_count {
			max_count = size
		}
	}
	fmt.Println(max_count)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

// Return all the points in ps that are nearest [x,y]
func nearest(x, y int, ps []Point) []Point {
	var res []Point
	min_d := 999999999
	for _, p := range ps {
		d := abs(x-p.x) + abs(y-p.y)
		if d > min_d {
			continue
		}
		if d == min_d {
			res = append(res, p)
		}
		if d < min_d {
			res = []Point{p}
			min_d = d
		}
	}
	return res
}
