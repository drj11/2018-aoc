package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

var Fabric [1000][1000]int

type Rect struct {
	id            int
	left, top     int
	width, height int
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	var rs []Rect
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		var r Rect
		fmt.Sscanf(line, "#%d @ %d,%d: %dx%d", &r.id, &r.left, &r.top, &r.width, &r.height)
		rs = append(rs, r)
	}

	for i := 0; i < len(rs); i += 1 {
		claim(rs[i])
	}

	total := 0
	for y := 0; y < len(Fabric); y += 1 {
		for x := 0; x < len(Fabric[0]); x += 1 {
			if Fabric[y][x] > 1 {
				total += 1
			}
		}
	}
	fmt.Println(total)

	for i := 0; i < len(rs); i += 1 {
		if max_claims(rs[i]) == 1 {
			fmt.Println(rs[i].id)
			break
		}
	}
}

// the maximum number of claims on the fabric covered by r
func max_claims(r Rect) int {
	max_claims := -1
	for y := r.top; y < r.top+r.height; y += 1 {
		for x := r.left; x < r.left+r.width; x += 1 {
			max_claims = max(Fabric[y][x], max_claims)
		}
	}
	return max_claims
}

func claim(r Rect) {
	for y := r.top; y < r.top+r.height; y += 1 {
		for x := r.left; x < r.left+r.width; x += 1 {
			Fabric[y][x] += 1
		}
	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
