package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

const N = 360

var Fabric [N][N]int

type Point struct {
	x, y int
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	inp := bufio.NewReader(file)

	var ps []Point
	for {
		line, err := inp.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		var p Point
		fmt.Sscanf(line, "%d,%d", &p.x, &p.y)
		ps = append(ps, p)
	}

	area := 0
	for y := 0; y < N; y += 1 {
		for x := 0; x < N; x += 1 {
			total := sum_manhattan(x, y, ps)
			if total < 10000 {
				area += 1
			}
		}
	}
	fmt.Println(area)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

// Return the sum of manhattan distances from x,y over all
// points in ps
func sum_manhattan(x, y int, ps []Point) int {
	sum := 0
	for _, p := range ps {
		sum += abs(x - p.x)
		sum += abs(y - p.y)
	}
	return sum
}
